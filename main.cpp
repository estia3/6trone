/*
 * Copyright (c) 2018, CATIE
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "mbed.h"
Thread thread1 ,thread2;
Mutex mutex;

void ping(){

	for(int i=0;i<10;i++){
		mutex.lock();
		printf("ping \n");
		mutex.unlock();

	}


}
void pong(){

	for(int i=0;i<10;i++){
		mutex.lock();
		printf("pong\n");
		mutex.unlock();
	}
}
int main(){
	thread1.start(ping);
	thread2.start(pong);
    while (true) {
    	printf("Alive! \n");
        ThisThread::sleep_for(500);
    }
}

